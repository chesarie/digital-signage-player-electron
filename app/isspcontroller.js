
	//ISSP Controller
$(function() {

var _screenOnline = false;

ISSP.Alive();
let CHECK_STATUS;
fs.exists(assetMainDir, (exists) => {
		if(!exists) {
			fs.mkdir(assetMainDir);
		}
	
});
if(localStorage.index)
	LASTINDEX = localStorage.index;
else
	LASTINDEX = 0;


/*isOnline(function(err, online) { 
	if(online) {
		console.log("startOnlinePlayer");
		if(localStorage.Log)
			UI.PushOfflineLog();


		if(sessionStorage.Reload) {
			UI.PageLoader("close");
			//$.post("http://localhost:8080/?action=exitpop", function() {});
			ISSP.UpdateScreenDisplay();
			setInterval(function() {ISSP.CheckSchedule()}, 10000);
		} else {
			startOnlinePlayer();
		}
		
	} else {
		console.log("startOfflinePLayer");
	
		startOfflinePlayer();
	}

});*/

setTimeout(function() { if(_screenOnline==false) {startOfflinePlayer();}}, 6000);

  window.addEventListener('online',  alertOnlineStatus)
  window.addEventListener('offline',  alertOnlineStatus)

function alertOnlineStatus() {
	isOnline(function(err, online) { 
		if(online) {
			location.reload();
		} 
	});
}
	
function startOnlinePlayer() {

		UI.PageLoader("open");
		//setTimeout(function() {
			setTimeout(function() {
				if(localStorage.Key) {

								ISSP.ValidateKey(localStorage.Key, function(res) { 
									setInterval(function() {ISSP.CheckSchedule()}, 10000);
									getScreenUpdates();
								});
				
				} else {
					UI.ValidationForm("open");
					UI.PageLoader("close");
				}
			}, 500);
	/*} else {
		$("#isspContent").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> No data to load. Login to ISSP Admin and drag schedule for this smartscreen. And then reset!</div>');
	}*/
}

function startOfflinePlayer() {
	
	Global.templateZones= JSON.parse(localStorage.templateZones);
		//Global.zoneSchedule = JSON.parse(localStorage.zoneSchedule);
	if(localStorage.screen_data) {
		ISSP.filterData(localStorage.screen_data);
		
		if(UI.PopulateZones()) {
			setTimeout(function() {
				ISSP.GetAssets();
				UI.PageLoader("close");
			}, 3000);
		}
	} else {
		alert("Please check internet connection!");
	}
	setInterval(function() {
		isOnline(function(err, online) { 
			if(online) {
				location.reload();
			}

		});
	}, 2400000);
	
}


		var socket = io("https://isspserver",{path: '/server1'});
		socket.on( 'connect', function() {
				console.log("connected !!!");
				_screenOnline = true;
				if(localStorage.Log)
					UI.PushOfflineLog();

					if(sessionStorage.Reload) {
						UI.PageLoader("close");
						//$.post("http://localhost:8080/?action=exitpop", function() {});
						ISSP.UpdateScreenDisplay();
						setInterval(function() {ISSP.CheckSchedule()}, 10000);
					} else {
						startOnlinePlayer();
					}
			});
		socket.on( 'disconnect', function() {
				console.log("Got disconnect! !!!");
			});
		socket.on( 'error', function() {
			console.log("Error Connection!!!");
		});
		socket.on('screen', function (data) {
			console.log("reset screen");
			//console.log("New content update");
			console.log(localStorage.screenID +":"+ data.screen_id);
			if(localStorage.screenID == data.screen_id) {
				//alert("test");
				location.reload();
				//console.log("New content update23");
				//ISSP.CheckNewMedia();
			}
		});


		socket.on('publishing crawler', function (data) {
			console.log("publishing crawler");
			console.log(data);
			if(data.screens.indexOf("" +localStorage.screenID+ "") != -1) {
				 var styles = {
			      backgroundColor : data.bgcolor,
			      color: data.color
			    };
			    
			    $( "#maincrawler marquee" ).html(data.text);
			    $( "#maincrawler" ).show();
     			$( "#maincrawler" ).css( styles );

     			setTimeout(function() {$( "#maincrawler" ).hide();}, parseInt(data.duration));
			}
		});
		
		socket.on('resetscreen', function (data) {
			console.log("resetscreen");
			console.log(localStorage.Key+"::"+data.screen_key);
			if(localStorage.Key == data.screen_key) {
				location.reload();
			} else {
				console.log("false resetscreen");
			}
		});

		socket.on('rebootscreen', function (data) {
			console.log("rebootscreen");
			console.log(localStorage.Key+"::"+data.screen_key);
			if(localStorage.Key == data.screen_key) {
				$.post("http://localhost:8080/?action=reboot", function() {});
			}
		});

		socket.on('Clean screen assets', function (data) {
			if(localStorage.Key == data.screen_key) {
				ISSP.Clean(assetMainDir);
			}
		});

		socket.on('Enable iiam', function (data) {
			if(localStorage.Key == data.screen_key) {
				localStorage.iiam = 1;
			}
		});

		socket.on('Disable iiam', function (data) {
			if(localStorage.Key == data.screen_key) {
				delete localStorage.iiam;
			}
		});


		socket.on('Keep Alive', function (data) {
			ISSP.Alive();
		});



  //UI Event 
$( "body" ).dblclick(function() {
  		var r = confirm("Reset player validation key?");
	if (r == true) {

	  			 UI.ValidationForm("open");
				UI.PageLoader("close");
	}
  });

  $("#screenkey").on('change', function() { $(".messageAlert").hide(); })
  $("#ValidateKeyBtn").on('click', function() {
  		var key = $("#screenkey").val();
  		$(this).button('loading');

  		if(key) {	
  			ISSP.ValidateKey(key, function(res) { 
  				if(res == true) {
  					delete localStorage.index;
  					getScreenUpdates();
  				} else {
  				
  					UI.invalidKeyMessage(Global.invalidKey);
  				}
  					
  			});
  		} else {
  			UI.invalidKeyMessage(Global.invalidKey);
  		}
  		$(this).button('reset');
  		return false;
		
	});


  function getScreenUpdates() {
		ISSP.GetZones(function(res) { 
			UI.ValidationForm("close"); 
			if(UI.PopulateZones()) {
				UI.PageLoader("open");
				if(ISSP.SaveZones()) {
						  ISSP.GetAssets();
				}
				setTimeout(function() {
						console.log("Read asset herer");
						//ISSP.ReadAssets();
						UI.PageLoader("close");
					}, 1000);
			
			}
		}); 
  }

  
});
