var UI = (function (obj) {
	obj.ValidationForm = function (action) {
		if(action == "open")
			$("#formModal").modal("show");
		else 
			$("#formModal").modal("hide");
		
	};
	obj.PageLoader = function (action) {
		if(action == "open")
			$("#opener").show();
		else 
			$("#opener").hide();
		
	};
	obj.invalidKeyMessage = function(message) {
		$(".messageAlert").html(message);
		$(".messageAlert").show();
	}
	obj.PopulateZones = function() {
		var obj = Global.templateZones;
		var html;
		if(obj) {
			$("#isspContent").html("");
			if(obj.length > 0) {
				for(var i = 0; i < obj.length; i++) {
					html = '<div class="zone" id="'+obj[i].unique_id+'" data-width="'+obj[i].zone_width+'" data-height="'+obj[i].zone_height+'" style="width: '+obj[i].zone_width+'px; height: '+obj[i].zone_height+'px; top:'+obj[i].pos_y+'px; left:'+obj[i].pos_x+'px;"></div>';
					$("#isspContent").append(html);
				}
				return true;
			} else {
				return false;
			}
		}
		
	};

	obj.Play = function(index, zonename, data) {
		

		var container = $('#'+zonename);
		var v;
		var next = parseInt(index)+1;
		if(index != 0) {
			localStorage.index = next;
		}
		console.log("index: "  + localStorage.index);
		if(index < data.length) {
				ISSP.CheckAsset(data[index].media_name, data[index].type, function(res) {
					//console.log(data[index]);
					if(data[index].type) {
						var assetToPlay = res.file;
						var width = container.attr("data-width");
						var height = container.attr("data-height");
						var _effect = UI.getRandomEffect();
						if(data[index].type.search('video') != -1){
				
							var html = '<video class="animated '+_effect+'" src="'+assetToPlay+'" width="'+width+'" height="'+height+'" autoplay/>'
							container.html(html);
							v = $('#'+zonename + " video").get(0);
							/*v.onerror = function() {
  									//console.log("Error " + v.error.code + "; details: " + v.error.message);
  									fs.unlink(assetToPlay);
  									UI.Play(next, zonename, data);
							}*/
							$('#'+zonename + " video").bind("ended", function() {
								$('#'+zonename + " video").remove();
									UI.Play(next, zonename, data);
							 });

							setTimeout(function() {
								$('#'+zonename + " video").remove();
									UI.Play(next, zonename, data);
							}, parseInt(data[index].interval));

						} else if(data[index].type.search('image') != -1) {
							
					
							var html = '<img class="animated '+_effect+'" alt="'+assetToPlay+'" src="'+assetToPlay+'" width="'+width+'" height="'+height+'" onerror="imgError(this);"/>'
							container.html(html);
							setTimeout(function() {
								$('#'+zonename + " img").remove();
								
								//alert(next);
								UI.Play(next, zonename, data);
							}, parseInt(data[index].interval));
						}  else if(data[index].type == "inline") {
							//console.log("inline: " + data[index].media_name);
							var html = '<webview class="animated '+_effect+'" align="middle" id="_inframe" allowtransparency="true" src="'+data[index].media_name+'" seamless></webview >'
							container.html(html);
							setTimeout(function() {
								$('#'+zonename + " webview").remove();
								UI.Play(next, zonename, data);
							}, parseInt(data[index].interval));
						} else if(data[index].type == "mswlive") {
							//msw version 
							setTimeout(function() {
								sessionStorage.FIREFOX =1;
								$.post("http://localhost:8080/?action=pop&url="+data[index].media_name, function() {});
							}, 2000);
							setTimeout(function() {
									delete sessionStorage.FIREFOX;
								$.post("http://localhost:8080/?action=exitpop", function() {});
								UI.Play(next, zonename, data);
							}, parseInt(data[index].interval));
						} else if(data[index].type == "crawler") {
							//console.log($('.bottom-crawler').length);
							if($('.bottom-crawler').length == 0) {
								var html = '<div class="bottom-crawler animated bounceInUp"><iframe allowtransparency="true" src="http://.ph/playerdev/crawler.html?str='+data[index].media_name+'" seamless></iframe></div>';
								$("#isspWidgets").html(html);
							}
							
							UI.Play(next, zonename, data);
							setTimeout(function() {
								$('.bottom-crawler').remove();
							}, data[index].interval);
						} else if(data[index].type == "fullscreen") {
							console.log($('.fullscreen-widget').length);
							if($('.fullscreen-widget').length == 0) {
								var html = '<div class="fullscreen-widget animated zoomIn"><iframe align="middle" allowtransparency="true" src="'+data[index].media_name+'" seamless></iframe></div>';
								$("#isspWidgets").html(html);
							}
							
							setTimeout(function() {
								$('.fullscreen-widget').remove();
								UI.Play(next, zonename, data);
							}, data[index].interval);
						} else if(data[index].type == "jsonfeed") {
							var jsonUrl = data[index].media_name;
							console.log("jsonfeed");
							isOnline(function(err, online) { 
								if(online) {
									$.get(jsonUrl,function(res) {
										var obj = JSON.parse(res);
										var feed_type;
										localStorage['jsonfeed_'+zonename] = res;
										var mediaObj = JSON.parse(obj.media);
										if(mediaObj.length > 0) {
											var i = 0;                     //  set your counter to 1
											myLoop(100); 
												function myLoop (dt) {           //  create a loop function
												   setTimeout(function () { 
												  // console.log('json:'); 
												   		
												   feed_type = mediaObj[i].type;
												  var the_media = mediaObj[i];
												      ISSP.CheckAsset(mediaObj[i].url, mediaObj[i].type, function(res) { 

												      		if(feed_type.search('video') != -1) {
												      				var html = '<video class="animated '+_effect+'" src="'+res.file+'" width="'+width+'" height="'+height+'" autoplay/>'
												      		} else {
												      			var html = '<img class="animated '+_effect+'" src="'+res.file+'" width="100%" height="100%" />'
												      		}
												      		
															container.html(html);
															//console.log("online");
															//console.log(the_media);
															UI.JsonLog(the_media, zonename);

												      });	
												      i++;                     //  increment the counter
												      if (i < mediaObj.length) {    
												      		//console.log(i + "::" +  mediaObj[i].length);        //  if the counter < 10, call the loop function
												         myLoop(parseInt(the_media.interval));             //  ..  again which will trigger another 
												      }                        //  ..  setTimeout()
												   }, dt)
												}

											   
											
										}
									});
								} else {
									var obj = JSON.parse(localStorage['jsonfeed_'+zonename]);

										if(obj.media.length > 0) {
											var i = 0;                     //  set your counter to 1
												myLoop2(100); 
												function myLoop2(dt) {           //  create a loop function
												   setTimeout(function () {    //  call a 3s setTimeout when the loop is called
												   	 var the_media = obj.media[i];
												       ISSP.CheckAsset(obj.media[i].url, data[index].type, function(res) { 
												      		if(feed_type.search('video') != -1) {
												      				var html = '<video class="animated '+_effect+'" src="'+res.file+'" width="'+width+'" height="'+height+'" autoplay/>'
												      		} else {
												      			var html = '<img class="animated '+_effect+'" src="'+res.file+'" width="100%" height="100%" />'
												      		}
															container.html(html);
																console.log("offline");
															console.log(the_media);
															UI.JsonLog(the_media, zonename);
												      });
												      i++;                     //  increment the counter
												      if (i < the_media.length) {            //  if the counter < 10, call the loop function
												         myLoop2(parseInt(the_media.interval));             //  ..  again which will trigger another 
												      }                        //  ..  setTimeout()
												   }, dt)
												}

											   
											
										}
								}

							});
							
							setTimeout(function() {
								//$('.fullscreen-widget').remove();
								UI.Play(next, zonename, data);
							}, parseInt(data[index].interval));
						}
					} else {
						UI.Play(next, zonename, data);
					} //end if type 	
					UI.Log(data[index]);
				}); //close file checking
				
		} else {
			UI.Play(0, zonename, data);
			
		}
		

	};
	obj.Log = function(data) {
		//console.log(Global.seematic);
			var param = {};
			param.media_id = data.media_id;
			param.screen_id = Global.screenID;
			param.publisher_id = Global.screenID;
			param.site_id = sessionStorage.site_id;
			param.logtime = moment().format('HH:00:00');
			param.logdate = moment().format('YYYY-MM-DD');
			var v = "?" + moment().format('DDMMYYYYh:mm:ss a');

			param.audienceSize = 0;
			param.males = 0;
			param.females = 0;
			param.unknown = 0;
			param.children = 0;
			param.adults = 0;
			param.seniors = 0;
			param.male_children = 0;
			param.male_adults = 0;
			param.male_seniors = 0;
			param.female_children = 0;
			param.female_adults = 0;
			param.female_seniors = 0;
			if(localStorage.iiam) {
					$.get(Global.seematic + v, function(xmlRes) {

						param.audienceSize = xmlRes.getElementsByTagName("audienceSize")[0].childNodes[0].nodeValue;

						param.males = xmlRes.getElementsByTagName("males")[0].childNodes[0].nodeValue;
						param.females = xmlRes.getElementsByTagName("females")[0].childNodes[0].nodeValue;
						param.unknown = xmlRes.getElementsByTagName("unknown")[0].childNodes[0].nodeValue;
						
						param.children = xmlRes.getElementsByTagName("children")[0].childNodes[0].nodeValue;
						param.adults = xmlRes.getElementsByTagName("adults")[0].childNodes[0].nodeValue;
						param.seniors = xmlRes.getElementsByTagName("seniors")[0].childNodes[0].nodeValue;

						param.male_children = xmlRes.getElementsByTagName("children")[1].childNodes[0].nodeValue;
						param.male_adults = xmlRes.getElementsByTagName("adults")[1].childNodes[0].nodeValue;
						param.male_seniors = xmlRes.getElementsByTagName("seniors")[1].childNodes[0].nodeValue;

						param.female_children = xmlRes.getElementsByTagName("children")[2].childNodes[0].nodeValue;
						param.female_adults = xmlRes.getElementsByTagName("adults")[2].childNodes[0].nodeValue;
						param.female_seniors = xmlRes.getElementsByTagName("seniors")[2].childNodes[0].nodeValue;
						
					});
			}
			//console.log(param);
			//setTimeout(function() {
				isOnline(function(err, online) {
	  			  if(online) {	
	  			  	//console.log("Thorw Log:");
	  			  	console.log(Global.server + 'media/log');
						$.post(Global.server + 'media/log', param, function(res) { 
							//console.log(res); 
						});
					} else {
						if(localStorage.Log) {
							localStorage.Log =  localStorage.Log + '::'  + JSON.stringify(param);
						} else {
							localStorage.Log =  JSON.stringify(param);
						}
							
					}
				});
			//}, 300);

			
			return;
	};

obj.PushOfflineLog = function() {

	let Logs = localStorage.Log.split("::");
	if(Logs.length > 0) {
		for(let i=0;i<Logs.length;i++) {
			$.post(Global.server + 'media/log', JSON.parse(Logs[i]), function(res) {});
		}
	}
	delete localStorage.Log;

};

obj.JsonLog = function(data, zonename) {
		//alert("log");
			var param = {};
			param.media_id = data.media_id;
			//params.campaign_id = data.campaign_id;
			param.screen_id = Global.screenID;
			param.publisher = zonename;

			var v = "?" + moment().format('DDMMYYYYh:mm:ss a');

			param.audienceSize = 0;
			param.males = 0;
			param.females = 0;
			param.unknown = 0;
			param.children = 0;
			param.adults = 0;
			param.seniors = 0;
			param.male_children = 0;
			param.male_adults = 0;
			param.male_seniors = 0;
			param.female_children = 0;
			param.female_adults = 0;
			param.female_seniors = 0;

			$.get(Global.seematic + v, function(xmlRes) {

				param.audienceSize = xmlRes.getElementsByTagName("audienceSize")[0].childNodes[0].nodeValue;

				param.males = xmlRes.getElementsByTagName("males")[0].childNodes[0].nodeValue;
				param.females = xmlRes.getElementsByTagName("females")[0].childNodes[0].nodeValue;
				param.unknown = xmlRes.getElementsByTagName("unknown")[0].childNodes[0].nodeValue;
				
				param.children = xmlRes.getElementsByTagName("children")[0].childNodes[0].nodeValue;
				param.adults = xmlRes.getElementsByTagName("adults")[0].childNodes[0].nodeValue;
				param.seniors = xmlRes.getElementsByTagName("seniors")[0].childNodes[0].nodeValue;

				param.male_children = xmlRes.getElementsByTagName("children")[1].childNodes[0].nodeValue;
				param.male_adults = xmlRes.getElementsByTagName("adults")[1].childNodes[0].nodeValue;
				param.male_seniors = xmlRes.getElementsByTagName("seniors")[1].childNodes[0].nodeValue;

				param.female_children = xmlRes.getElementsByTagName("children")[2].childNodes[0].nodeValue;
				param.female_adults = xmlRes.getElementsByTagName("adults")[2].childNodes[0].nodeValue;
				param.female_seniors = xmlRes.getElementsByTagName("seniors")[2].childNodes[0].nodeValue;
				
			});
			setTimeout(function() {

					isOnline(function(err, online) {
		  			  if(online) {
							$.post(Global.Jsonserver, param, function(res) {});
						} 
					});
			}, 500);
			return;
		
	};

	obj.getRandomInt = function() {
			//return Math.floor(Math.random() * (15000 - 10000 + 1)) + 10000;
			return 30000;
 
		
	};

	obj.getRandomEffect = function() {
			var _index = Math.floor(Math.random() * (7 - 1 + 1)) + 1;
			var effects = ['fadeIn', 'fadeIn', 'fadeIn' , 'fadeIn', 'fadeIn', 'fadeIn' , 'fadeIn', 'fadeIn'];
			return effects[_index];
 
		
	};

	obj.getInlineInterval = function(zonename) {
			//return Math.floor(Math.random() * (15000 - 10000 + 1)) + 10000;
			if(zonename = "zone005") {
				return 30000;
			} else {
				return 40000;
			}
			
	
		
	};

	return obj;
}(UI || {}));