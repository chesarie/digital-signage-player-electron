//if (require('electron-squirrel-startup')) return;
const  {app} = require('electron');
const  {BrowserWindow} = require('electron');
app.commandLine.appendSwitch('disable-web-security');
app.commandLine.appendSwitch('--disable-web-security');
app.on('ready', function () {
	var playerWindow = new BrowserWindow({
		backgroundColor: '#3398DA',
		fullscreen: true,
		frame: false,
		webPreferences: { 
			 nodeIntegration: true,
			 webSecurity: false
		}
	})
	playerWindow.openDevTools();
	//  var playerWindow = new BrowserWindow({width: 800, height: 600})
	playerWindow.once('ready-to-show', () => {
  			playerWindow.show()
	})
	playerWindow.loadURL('file://' + __dirname + '/index.html')
});