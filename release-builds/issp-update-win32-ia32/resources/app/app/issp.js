var ISSP = (function (obj) {
	obj.check_network_state = function () {
		HTTP.Ping(Global.server, function(res) {
			Global.isOnline = res;
			return res;
		});
	};

	obj.CheckAsset = function(http_asset, asset_type,  callback) {
		var returnVal = {};
		returnVal.file = http_asset;
		var assetFilter = http_asset.split('/');
		var filtered_asset = assetFilter[assetFilter.length - 1];
		var getProtocol = https;
		if(assetFilter[0] == "http:") {
			getProtocol = http;
		}
		var assetSavePath = assetMainDir + "/" + filtered_asset;
		if(asset_type == 'inline' || asset_type == 'fullscreen' || asset_type == 'crawler' || asset_type == 'jsonfeed') {
			callback(returnVal);
		} else {
			fs.exists(assetSavePath, (exists) => { 
				if(!exists) { 
					var assetDL = fs.createWriteStream(assetSavePath);
					var requestAsset = getProtocol.get(http_asset, function(response) {
						response.pipe(assetDL);
					});
					//console.log("file not exists");
					callback(returnVal);
					return;
				} else {
					//console.log("file already exists");
					returnVal.file = assetSavePath;
					callback(returnVal);
						return;
				}
			});
		}
		

	};

	obj.Validate = function () {
		if(Global.screenID)
			return true;
		else 
			return false;
	};

	obj.ValidateKey = function(key, callback) {
		Request.Validate(Global.server + 'screen/validatekey', key, function(res) { 
			//console.log(res);
			var obj = JSON.parse(res);
			if(obj.status > 0) {
				//console.log(obj.screen[0]);
				Global.licenseKey = key;
				Global.templateID = obj.screen[0].template;
				Global.screenObj = obj.screen[0];
				Global.screenID =  obj.screen[0].screen_id;
				sessionStorage.site_id = obj.screen[0].site_id;
				sessionStorage.group_schedule = obj.screen[0].group_schedule;
				//console.log("So fast");
				if(obj.screen[0].new_schedule) {
					localStorage.screen_data = obj.screen[0].new_schedule;
					ISSP.filterData(obj.screen[0].new_schedule);
				} else {
					if(sessionStorage.group_schedule && sessionStorage.group_schedule != "null") {
						var schedObjArry = sessionStorage.group_schedule.split("::");
						var sobj = JSON.parse(schedObjArry[schedObjArry.length - 1]);
						sessionStorage.load_schedule = sobj.display_time;
						localStorage.LastScheduleID = sobj.id;
						ISSP.PopulatePlaylistItem(localStorage.LastScheduleID);
					}
					
				}
				
				Global.Hash = md5(res);
				
				localStorage.Key = Global.licenseKey;
				localStorage.ZoneScheduleHash = JSON.stringify(md5(obj.screen[0].new_schedule));
				callback(true);
				 return;
			} else {
				callback(false); 
				return;
			}	
		});
		
	};
	obj._filterData = function(schedule) {
		//Global.zoneSchedule = obj.screen[0].new_schedule;
		let lastzonename="";
		let lastobj;
		let zoneObjArry = schedule.split("::");
		let _dataArray = [];
		let _fileArray = [];
		//let 
		//console.log(schedule.split("::"));
		//console.log(JSON.stringify(zoneObjArry));
		for(var z=0; z<zoneObjArry.length;z++) {
			//console.log(zoneObjArry[z]);
			let str = zoneObjArry[z].replace('"[', "[").replace(']"', "]"); //filter cahracter
			let zoneObj = JSON.parse(str);
			console.log(zoneObj);
			//let zonename = zoneObj.zonename;
			//let medias = zoneObj.data;

			if(lastzonename != zoneObj.zonename) {
				_fileArray = [];
				lastzonename = zoneObj.zonename;
				//console.log(JSON.stringify(zoneObj.data[0]));
				
				if(zoneObj.data.length) {
					for(var f=0; f<zoneObj.data.length;f++) { 
						_fileArray.push(JSON.stringify(zoneObj.data[f]));
					}
				}
				
				let _zone = {};
				_zone.zonename = zoneObj.zonename;
				_zone.data = _fileArray;
				_dataArray.push(_zone);

			} else if (lastzonename == zoneObj.zonename){
				if(zoneObj.data.length) {
					for(var f=0; f<zoneObj.data.length;f++) { 
						_fileArray.push(JSON.stringify(zoneObj.data[f]));
					}
				}

				let _zone = {};
				_zone.zonename = zoneObj.zonename;
				_zone.data = _fileArray;
				_dataArray.splice(z-1, 2,_zone);
			
			}
		
		}
		//console.log(_dataArray);
		let _zoneObjArry= [];
		let _UniName = "";
			if(_dataArray.length) {
				for(var x=0; x<_dataArray.length;x++) { 
					if(_UniName != _dataArray[x].zonename) {
						_zoneObjArry.push(JSON.stringify(_dataArray[x]).replace(/\\/g, ""));
						_UniName = _dataArray[x].zonename;
					}
					
					
				}
			}
			
		Global.zoneSchedule = _zoneObjArry;
		console.log(Global.zoneSchedule);
	};

	obj.filterData = function(schedule) {
		//console.log(schedule);
		var _dataArray;
		var lastzonename="";
		var lastobj;
		var zoneObjArry = schedule.split("::");
		let _zoneObjArry = [];
		//console.log(schedule);
		//console.log(JSON.stringify(zoneObjArry));
		for(var z=0; z<zoneObjArry.length;z++) {
			
			var str = zoneObjArry[z].replace('"[', "[").replace(']"', "]"); //filter cahracter
			//console.log(str);
			var zoneObj = JSON.parse(str);
			var zonename = zoneObj.zonename;
			var medias = zoneObj.data;
				//console.log(zoneObj);
			if(lastzonename != zoneObj.zonename) {
				lastzonename = zoneObj.zonename;
				lastobj = zoneObj.data;
			} else if (lastzonename == zoneObj.zonename){
				_dataArray = lastobj.concat(zoneObj.data);
				zoneObj.data = _dataArray;
				lastobj = zoneObj.data;
				_zoneObjArry.splice(z-1, 2, JSON.stringify(zoneObj));
			}
			_zoneObjArry.push(JSON.stringify(zoneObj));
			//console.log(_dataArray);
		
		}
		let _zoneMedia= [];
		let _UniName = "";
			if(_zoneObjArry.length) {
				for(var x=_zoneObjArry.length-1; x>=0;x--) {
					let _obj = JSON.parse(_zoneObjArry[x]);
					if(_UniName != _obj.zonename) {
						_zoneMedia.push(_zoneObjArry[x]);
						_UniName = _obj.zonename;
					}
				}
			}
		Global.zoneSchedule = _zoneMedia;
		//console.log(Global.zoneSchedule);
	};
	obj.CheckSchedule = function() {
		//console.log("Pulling data");
			ISSP.ValidateKey(localStorage.Key, function(res) { 
						var currenttime = moment().format("hh:mm A");
						//console.log(currenttime);

						if(sessionStorage.group_schedule && sessionStorage.group_schedule != "null") {
							var schedObjArry = sessionStorage.group_schedule.split("::");
							for(var i=0; i<schedObjArry.length;i++) {
								var obj = JSON.parse(schedObjArry[i]);
								if(obj.id) {
								//	console.log(currenttime +"::"+obj.display_time);
									if(obj.display_time == currenttime) {
											console.log("Its time!");
											
											
											if(sessionStorage.load_schedule != obj.display_time) {
												sessionStorage.load_schedule = obj.display_time;
												localStorage.LastScheduleID = obj.id;
												ISSP.PopulatePlaylistItem(localStorage.LastScheduleID);
											}
											
									}
								}
								
							}
						} else {
							$("#isspContent").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> No data to load. Login to ISSP Admin and drag schedule for this smartscreen. And then reset!</div>');
						}
				});


		
	};



	obj.PopulatePlaylistItem = function(scheduleID) {
		var params = {};
		var sched_id =0;
		if(sessionStorage.load_schedule && sessionStorage.load_schedule != "undefined") {
			if(scheduleID && scheduleID != 'undefined') {
				sched_id = scheduleID;
			}
			params.id= sched_id;
			params.s = Global.screenID;
			console.log('Get playlist');
			console.log(params);
			$.post(Global.server + 'schedule/playlist', params, function(res) {
				setTimeout(function(){ 
					console.log("New schedule to load!");
					sessionStorage.Reload = 1;
					 location.reload(); 
				}, 1000);
			});
		}
		
	};

	obj.UpdateScreenDisplay= function() {
		console.log("UpdateScreenDisplay");
		ISSP.ValidateKey(localStorage.Key, function(res) { 
			ISSP.GetZones(function(res) { 
				UI.ValidationForm("close"); 
				if(UI.PopulateZones()) {
					//UI.PageLoader("open");
					if(ISSP.SaveZones()) {
							 ISSP.GetAssets();
					}
				
				}
			}); 
		});
		
	};

	obj.GetZones = function (callback) {
		Request.Zones(Global.server + 'template/get', function(res) {
			var obj = JSON.parse(res);
			//localStorage.ZoneHash = md5(res);
			localStorage.templateZones = JSON.stringify(obj.zones); 
			Global.templateZones = obj.zones;
			callback(true);
			return;
		});
		
	};

	obj.GetAssets = function(callback) {
		
		//console.log(Global.zoneSchedule);
		
		var zoneObjArry = Global.zoneSchedule;
		for(var z=0; z<zoneObjArry.length;z++) {
			var str = zoneObjArry[z].replace('"[', "[").replace(']"', "]"); //filter cahracter
			//console.log(str);
			var zoneObj = JSON.parse(str);
			var zonename = zoneObj.zonename;
			var medias = zoneObj.data;
			//console.log(zoneObj.zonename);
			ISSP.ComposeAsset(zonename, medias);
		}
		//callback(true);
	};

	obj.Log = function(params) { 

	};
	obj.ComposeAsset = function(zonename, medias) { 

			if(medias.length > 0)
				UI.Play(0, zonename, medias);

	};
	obj.ReadAssets = function() {
		
		var templateZones = Global.templateZones;
		//console.log(templateZones);
		var results = [];
		var zonename;
		for(var i=0; i < templateZones.length; i++) {
			zonename = templateZones[i].unique_id;
			var data = ISSP.genData(zonename);
			//console.log(data);
			if(data.length > 1)
				UI.Play(0, zonename, data);
		}
		
	};

	obj.genData = function(zonename) {
		var zoneData = Global.ArrZone;
		//console.log(zoneData);
		var data = [];
		if(zoneData.length > 0) {
			for(var i = 0;i < zoneData.length; i++) {
				if(zoneData[i].zonename == zonename) {
					data.push(zoneData[i]);
				}

			}
		}
		return data;
	};

	obj.SaveZones = function() {
		
		localStorage.templateID =	Global.templateID;
		localStorage.screenObj = JSON.stringify(Global.screenObj);
		localStorage.screenID =  Global.screenID;
		localStorage.zoneSchedule = Global.zoneSchedule;
		localStorage.Hash =	Global.Hash;
		localStorage.Key = Global.licenseKey;
	   	return true;
	};
	obj.CheckNewMedia = function() {
		ISSP.GetAssets();
		setTimeout(function() {
			ISSP.ReadAssets();
		}, 3000);
	};
	obj.Clean = function(dirPath) {
		 try { var files = fs.readdirSync(dirPath); }
      catch(e) { return; }
      if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
          var filePath = dirPath + '/' + files[i];
          if (fs.statSync(filePath).isFile()) {
          	console.log("rm:" + filePath);  fs.unlinkSync(filePath);
          } else {
          	 rmDir(filePath);
          }
           
        }
      fs.rmdirSync(dirPath);
	};
	obj.Alive = function() {
		if(localStorage.screenID) {
			 var param = {};
        	 param.screen_id = localStorage.screenID;
        	param.status = 3;
        	param.last_update = moment().format("YYYY-MM-DD HH:mm:ss");
	        $.post(Global.server + "screen/status", param, function(res) {});
		}
	};
	obj.Location = function() {
		geolocation.getCurrentPosition(function (position) {
  
  console.log(position);
});
	}

	return obj;
}(ISSP || {}));